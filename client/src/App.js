import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Redirect, NavLink } from "react-router-dom";
import './App.css';
import './index.css';
import axios from 'axios';
import {URL} from './config.js';




import NavBar from './components/NavBar.js';
import Footer from './components/Footer.js';
import Stripe from './components/stripe.js'

import Home from './modules/Home.js'
import GenrePage from './modules/GenrePage.js'
import SingleProductPage from './modules/SingleProductPage.js'
import Bestsellers from './modules/Bestsellers.js'
import CreateAccountPage from './modules/CreateAccountPage.js'
import CRUDadmin from './modules/CRUDadmin.js'
import CartPage from './modules/CartPage.js'
import Login from './modules/Login.js'
import Register from './modules/Register.js'
import UserPage from './modules/UserPage.js'

import PaymentSuccess from "./modules/payment_success";
import PaymentError from "./modules/payment_error";



function App() {

  const [products, setProducts] = useState([])
  const [isLoggedIn, setIsLoggedIn] = useState(false)
  const [isAdmin, setIsAdmin] = useState(false)
  const [userId, setUserId] = useState('')
  const token = JSON.parse(localStorage.getItem('token'))

  const verify_token = async () => {
    if( token === null ) return setIsLoggedIn(false)
    try {
      axios.defaults.headers.common['Authorization'] = token
      const response = await axios.post(`${URL}/users/verify_token`)
      if (response.data.succ.admin) { setIsAdmin(true)} else { setIsAdmin(false)}
      return response.data.ok ? setIsLoggedIn(true) : setIsLoggedIn(false)
     }
     catch(error){
        console.log(error)
    }
  }

  useEffect(() => {
    verify_token();
    getBook();
    console.log('isLoggedIn: ', isLoggedIn)
  }, [])


  const login = (token, id) => {
    console.log('token ==>')
    localStorage.setItem('token', JSON.stringify(token))
    setUserId(id)
    setIsLoggedIn(true)
  }

  const logout = () => {
    localStorage.removeItem('token');
    setIsLoggedIn(false)
  }

  let getBook = async () => {
    try {
      let response = await axios.get(`${URL}/products`)
      // console.log('Home response: ', [response.data])
      setProducts([...response.data.myProduct])
    } catch (error) {
      console.log(error)
    }
  }



  
  return (
      <Router>
        
        <div className='topnavbar'>
        <NavBar  isAdmin={isAdmin} isLoggedIn={isLoggedIn} userId={userId}/>
        </div>

        <div className='hamburger'>
          <span>|||</span>
          <div className='hamburger-content'>
          <NavBar  isAdmin={isAdmin} isLoggedIn={isLoggedIn} userId={userId}/>
          </div>
        </div>  

        <Route exact path='/users'   render={ props =>  isLoggedIn ? <Redirect to={'/'}/> : <Login login={login} {...props}/>} />
        <Route exact path='/users/register' render={ props => <Register {...props}/>}/>
        <Route exact path='/users/CRUDadmin' render={ props => {return !isAdmin ? <Redirect to={'/'}/> : <CRUDadmin logout={logout} {...props}/>}}/>
        <Route exact path='/users/userpage/'   render={ props =>  !isLoggedIn ? <Redirect to={'/'}/> : <UserPage logout={logout} products={products} {...props}/>}/>

          <Route exact path="/" render = {(props) => <Home {...props}/>}/>
          <Route path="/genre/:genre" render = {(props) => <GenrePage {...props}/>} />
          <Route exact path="/bestsellers" render = {(props) => <Bestsellers {...props}/> } />
          <Route path="/book/:title" render = {(props) => <SingleProductPage  {...props} isLoggedIn={isLoggedIn}/> }  />
          <Route exact path='/createaccount' component={CreateAccountPage}/>
          <Route exact path='/cart' render = {(props) => <CartPage {...props} products={products}/> } />
          
          <Route exact path="/stripe" render={props => <Stripe {...props} products={products} />} />
          <Route exact path="/payment/success" render={props => <PaymentSuccess {...props} />}/>
          <Route exact path="/payment/error" render={props => <PaymentError {...props} />}/>
        
        <Footer/>
      </Router>
  )    
}

export default App;