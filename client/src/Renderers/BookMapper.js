// List

import React from 'react';
import ProductRenderer from '../Renderers/ProductRenderer.js';



let BookMapper = props => {
  // console.log('BookMapper.props: ', props)
  let renderedBook = props.Library.map((book, i) => {
    // console.log('bookMapper.book: ', book)
    return <ProductRenderer key={i} {...props} book={book}/>
  })
  // console.log('rendered book: ', renderedBook)

  return <div>{renderedBook}</div>
}
 export default BookMapper 