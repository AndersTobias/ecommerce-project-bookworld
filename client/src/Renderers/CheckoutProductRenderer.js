import React from "react";

function Product(props) {
  return (
      <div className="checkout_product_container">
        <div className="checkout_image_container">
          <img alt="cover" className="checkout_image" src={props.book.images} />
        </div>
        <div className="checkout_text_container">
          <h2>{props.book.name}</h2>

          <p>quantity : {props.book.quantity}</p>

          <p>price : {props.book.amount} €</p>

          <p>total price : {props.book.amount * props.book.quantity} €</p>
        </div>
      </div>
  );
};

export default Product;
