import React, {useState, useEffect} from 'react';
import moment from 'moment'

function OrderRenderer(props) {
  
  // console.log('OrderRenderer.props: ', props)

  let orderDate = moment(props.order.date).format('YYYY-DD-MM')
  let orderTime = moment(props.order.date).format('HH:MM:SS')

  let total = 0;

  return (
  <div className='order'>
    <h4>Order number:{props.order._id}</h4>
    <p>Date: {orderDate} Time: {orderTime}</p>
    <p>Items:</p>
    { 
      props.order.cart.map((cartItem, i) => {
        
        return (
          props.products.map((productItem, i) => {
            
            // console.log('OrderRenderer.props.products.id: ', productItem._id, 'OrderRenderer.cartITem.id: ', cartItem.id)
            if (cartItem.id === productItem._id) {
              total += cartItem.amount*productItem.price;
              // console.log('yippee');
              return <div>
                {productItem.title} x{cartItem.amount} {productItem.price}$ <br/>
                
                </div>
          }
        })
        )
      })
    }
    <p>Total cost: {total}$</p>
  </div>
  )
}

export default OrderRenderer