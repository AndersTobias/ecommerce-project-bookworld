import React, {useState, useEffect} from 'react';
import cartPic from '../Misc/cart.png';
// import starPic from '../Misc/star.png';



function ProductRenderer(props) {

  //! ------------- Cart

  // const [cart, setCart] = useState([]);
  const [message, setMessage] = useState('')

  useEffect(() => {
    const timer = setTimeout(() => {setMessage('')}, 2000);
    return() => clearTimeout(timer);
  }, [message]);

  let toLocalStorage = () => {
    let tempcart = JSON.parse(localStorage.getItem("cart")) || [];
    let cartBoolean = false; 

    // console.log('tempcart.length : ' , tempcart.length , 'map :' , tempcart.map((book, i) => book.title), ' tempcart : ', tempcart, ' props.book.title : ', props.book.title)

    tempcart.map((book, i) => { 
      if(book.id.indexOf(props.book._id) !== -1) {cartBoolean = true}
    })  

    if(cartBoolean === false) {tempcart.push({id: props.book._id, amount: 1});} //!<----------------------- here

    if(tempcart.length === 0) {tempcart.push({id: props.book._id, amount: 1});} //!<----------------------- here

    localStorage.setItem("cart", JSON.stringify(tempcart));

    setMessage(props.book.title + ' has been added to your cart')

    return console.log('item added to cart in localstorage')
  };

  //!--------------------


    return (
      <div className='productFull'>
        <div>
          <img src={props.book.cover.photo_url} alt='Cover' className='coverPic'/>

          <h3>{props.book.genre}</h3>
          <h3 title='Calculated at an average reading speed of 225 words per minute'>Aproximately {(props.book.wordCount/225/60).toFixed(2)} hours to read</h3>
          <h2>Price: {props.book.price}$</h2> 
          {props.book.bestseller === true && <h3 className= 'bestseller'>Bestseller!</h3>}
        </div>
        <div>
          <h1 onClick={() => props.history.push(`/book/${props.book.title}`)}>{props.book.title}</h1>
          <h2>{props.book.author}</h2>
          <h2>Description:</h2>
          <p>{props.book.description}</p>
          <div className='smallPics'>
            <img src={cartPic} onClick={toLocalStorage} alt='Add to Cart' className='cartPic' title='Add to cart'/>
            {/* <img src={starPic} href='/' alt='Add to wishlist' className='starPic'/> */}
            <p className='added'>{message}</p>
          </div>
        </div>
      </div>
    )
}

export default ProductRenderer;