// List

import React from 'react';
import ReviewRenderer from '../Renderers/ReviewRenderer.js';



let ReviewMapper = props => {

  // console.log('reviewMapper.props ', props )

  let renderedReview = props.Library.map((book, idx) => {
    return <div> 
      <h4>Reviews of {book.title}:</h4>
      {book.reviews.map((review, i) => {
      return <div>
        <ReviewRenderer key={i} {...props} review={review}/>
        </div>
    })}</div>
  })
  // console.log('rendered review: ', renderedReview)
  return <div>{renderedReview}</div>
}

export default ReviewMapper 




// let ReviewMapper = props => {

//   // console.log('reviewMapper.props ', props )

//   let renderedReview = props.Library.map((book, idx) => {
//     // console.log('reviewMapper.props.book ', book )
//     return book.reviews.map((review, i) => {
//       // console.log('reviewMapper.props.book.review ', review )
//       return <div>
//         <h4>{book.title}:</h4>
//         <ReviewRenderer key={i} {...props} review={review}/>
//         </div>
//     })
//   })
//   // console.log('rendered review: ', renderedReview)
//   return <div>{renderedReview}</div>
// }