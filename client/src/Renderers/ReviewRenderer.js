import React from 'react';
import bookstack from '../Misc/bookstack.png'

function ReviewRenderer(props) {
  // debugger;
  // console.log('reviewRenderer.props ', props )
  // console.log('reviewRenderer is running' )

  return (
    <div className='review'>
      <div>
        <h2>By: {props.review.user}</h2>
        <p>{props.review.review}</p>
      </div>
      <div className='reviewStars'>  
        {props.review.stars >= 1 && <img src={bookstack} alt='1 Book out of 5'/>}
        {props.review.stars >= 2 && <img src={bookstack} alt='2 Book out of 5'/>}
        {props.review.stars >= 3 && <img src={bookstack} alt='3 Book out of 5'/>}
        {props.review.stars >= 4 && <img src={bookstack} alt='4 Book out of 5'/>}
        {props.review.stars >= 5 && <img src={bookstack} alt='5 Book out of 5'/>}
      </div>
    </div>
  )
}
  


export default ReviewRenderer;