import React from 'react';

let Footer = () => {
return (
    <footer id='footer'>
    <div><p><a href="https://www.facebook.com/at.nicolaysen">Social media</a></p></div>
    <div><p><a href="mailto:BookWorld.com">BookWorld.com</a></p></div>
    <div><p><a href="tel:">+34 222 333 232</a></p></div>
  </footer> )}

 export default Footer 