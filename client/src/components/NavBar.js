import React from 'react';
import { NavLink } from "react-router-dom";



let NavBar = (props) => {
return (
  <div id="navBar">
    <ul>
    <NavLink className='NavLink' exact to={"/"}>Home</NavLink>

    <span className='NavLink' >
      <div className="genreDropdown">
        <button id='genreButton'>Genres</button>
        <div id="genreNavBar">
          <NavLink exact to={"/genre/Horror"}>Horror</NavLink>
          <NavLink exact to={"/genre/Fantasy"}>Fantasy</NavLink>
          <NavLink exact to={"/genre/Crime"}>Crime</NavLink>
          <NavLink exact to={"/genre/Realism"}>Realism</NavLink>
          <NavLink exact to={"/genre/SciFi"}>Sci-Fi</NavLink>
        </div>
      </div>
    </span>

    <NavLink className='NavLink' exact to={"/bestsellers"}>Bestsellers</NavLink>

    <NavLink className='NavLink' exact to={"/cart"}>Cart</NavLink>

    {props.isLoggedIn === false ? ( <NavLink className='NavLink' exact to={"/users/"}>Login/Register</NavLink> ) : null }

    {props.isLoggedIn === true ? ( <NavLink className='NavLink' exact to={`/users/userpage/`}>My Account</NavLink> ) : null } 


    {/* <input/> */}
    {props.isAdmin === true ? ( <NavLink className='NavLink' exact to={"/users/CRUDadmin"}>Admin</NavLink>) : null }

    </ul>
  </div>
)}

 export default NavBar 

