import React from "react";
import Checkout from "../modules/checkout.js";
import { StripeProvider, Elements } from "react-stripe-elements";

const Stripe = props => {
  console.log(process.env.REACT_APP_STRIPE_PUBLIC_KEY)
  return (
    <StripeProvider apiKey={process.env.REACT_APP_STRIPE_PUBLIC_KEY}>
      <Elements>
        <Checkout {...props} products={props.products}/>
      </Elements>
    </StripeProvider>
  );
};

export default Stripe;
