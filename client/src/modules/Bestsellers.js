import React, {useEffect, useState} from 'react';
import BookMapper from '../Renderers/BookMapper.js'; 
// import ReviewRenderer from '../Renderers/ReviewRenderer.js';
import Axios from 'axios'
import {URL} from '../config.js'



const Bestsellers = (props) => {

  const [bookState, setBookState] = useState([]);

  useEffect(() => {
    let getBook = async () => {
      try {
        let response = await Axios.get(`${URL}/products/bestsellers`)
        // console.log('Home response: ', [response.data])
        setBookState([...response.data.myProduct])
      } catch (error) {
        console.log(error)
      }
    }
    getBook()
  }, [])

  return (
    <div>
      <h1 className='pageTitle'>These are this weeks Bestselllers:</h1>
      <section class='allBooks'>
        <BookMapper {...props} Library={bookState}/>
      </section>
      {/* <section>
        <ReviewRenderer Library={bookState}/>
      </section> */}
    </div>
  )
}

export default Bestsellers