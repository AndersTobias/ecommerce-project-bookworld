import React, {useState} from "react";
import Axios from 'axios'
import UploadImages from '../UploadImages' //! ---
import widgetStyle from '../widgetStyle'
import {URL} from '../config.js'


const CRUDadmin = (props) => {

  const [ form , setValues ] = useState({
    title: '',
    newTitle: '',
    author: '',
    genre: '',
    price:'',
    description:'',
    cover:'',
    wordCount:'',
    stock:'',
  })

  const [imageUpload, setImageUpload] = useState()
  
  const [ message , setMessage ] = useState('') //!  <----------------------- All "here" refer to the set message  
  
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
  }
  
	const handleSubmitNew = async (e) => {
    e.preventDefault()
    if(!imageUpload) {
      setMessage('Image required')
      return null;
    } 
		try{
      const response = await Axios.post(`${URL}/products/new`,{
        title: form.title,
        author: form.author,
        genre: form.genre,
        price: form.price,
        description: form.description,
        cover: imageUpload, //! <-----------image upload
        wordCount: form.wordCount,
        stock: form.stock, //! <-- stock
      })
      setMessage(response.data.message) //!  <----------------------- here set  message
      setImageUpload();//! <--- return state to blank ffor the image uploader
		}
    catch(error){
      console.log(error)
    }
    document.forms["addForm"].reset(); //! <----- to reset form after submit
	}

  const handleDelete = async (e) => {
		e.preventDefault()
		try{
      const response = await Axios.post(`${URL}/products/delete`,{
        title: form.title,
      })
      setMessage(response.data.message) //!  <----------------------- here
		}
    catch(error){
      console.log(error)
    }
    document.forms["deleteForm"].reset(); //! <----- to reset form after submit
  }
  
	const handleUpdate = async (e) => {
		e.preventDefault()
		try{
      const response = await Axios.post(`${URL}/products/update`,{
        title: form.title,
        newTitle: form.newTitle,
        author: form.author,
        genre: form.genre,
        price: form.price,
        description: form.description,
        wordCount: form.wordCount,
        stock: form.stock //! <-- Stock
      })
      setMessage(response.data.message) //!  <----------------------- here
		}
    catch(error){
      console.log(error)
    }
    document.forms["updateForm"].reset(); //! <----- to reset form after submit
	}


 return <div className='CRUDadmin'>
    <h1>C.R.U.D.</h1>
    <h3>Add product to DB</h3>
    <form onSubmit={handleSubmitNew}
          onChange={handleChange}
          className='CRUD' id='addForm'> {/*  <------- add id */}
      <label>
        Book Title: 
      </label>
      <input type="text" name="title"/><br/>

      <label>
        Author: 
      </label>
      <input type="text" name="author" /><br/>

      <label>
        Genre: 
      </label>
      <input type="text" name="genre" /><br/>

      <label>
        Price: 
      </label>
      <input type= 'text' name="price" /><br/>

      <label>
        Description: 
      </label>
      <input type="text" name="description" /><br/>

      {/* <label>
        Cover: 
      </label>
      <input type="text" name="cover" /><br/> */}

      <label>
        Word count: 
      </label>
      <input type="text" name="wordCount" /><br/>

      <label>
        Stock: 
      </label>
      <input type="number" name="stock" /><br/> {/*  <------- Here */}

      <input className='CRUDsubmit'  type="submit" value="Add" />

      <div className='CRUDmessage'><h4>{message}</h4></div> {/*  <------- Here */}

    </form>

    <div className="CRUDimageUploadContainer" >
      <p>You need to upload an image when you <br/> add a new book to the library here:</p>
      <UploadImages setImageUpload={setImageUpload}/>
    </div>

    <h3>Delete product from DB</h3>
    <form onSubmit={handleDelete}
          onChange={handleChange}
          className='CRUD' id='deleteForm'> {/*  <------- add id */}
      <label>
      Product Name: 
      </label>
      <input type="text" name="title" /><br/>
      <input className='CRUDsubmit'  type="submit" value="Delete" />

      <div className='CRUDmessage'><h4>{message}</h4></div> {/*  <------- Here */}
    </form>



    <h3>Update a product in DB</h3>
    <form onSubmit={handleUpdate}
          onChange={handleChange}
          className='CRUD' id='updateForm'> {/*  <------- add id */}
      <label>
        Book Title:
      </label>
      <input type="text" name="title" /><br/>

      <label>
        New Book Title:
      </label><input type="text" name="newTitle" /><p id='CRUDwarn'>(If the title is to remain the same after update, then make sure that "Book Title" and "New Book Title" are the same.)</p><br/>

      <label>
        Author:
      </label><input type="text" name="author" /><br/>

      <label>
        Genre:
      </label><input type="text" name="genre" /><br/>

      <label>
        Price:
      </label><input type= 'text' name="price" /><br/>

      <label>
        Description:
      </label><input type="text" name="description" /><br/>

      {/* <label>
        Cover:
      </label><input type="text" name="cover" /><br/> */}

      <label>
        Word count:
      </label><input type="text" name="wordCount" /><br/>

      <label>
        Stock: 
      </label>
      <input type="number" name="stock" /><br/>

      <input className='CRUDsubmit' type="submit" value="Update" />
      <div className='CRUDmessage'><h4>{message}</h4></div> {/*  <------- Here */}
    </form>

    <button className='CRUDsubmitLogout' onClick={() => {
      props.history.push('/users');
      props.logout()
    }}>Logout</button>

  </div>
};
export default CRUDadmin;