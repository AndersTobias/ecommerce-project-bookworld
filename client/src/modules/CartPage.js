import React, { useState, useEffect } from "react";
// import axios from 'axios'
// import {URL} from '../config.js'
// import { injectStripe } from "react-stripe-elements";

const Cart = (props) => {
  
  const [ourCart, setOurCart] = useState([]);
  const [total, setTotal] = useState(0);


  let countTotal = () => {
    let count = 0;
    ourCart.map(book => {
      count += book.amount * book.price;
    });
    setTotal(count);
  };

  useEffect(() => {
    fSetTrueCart()
  }, [])

  useEffect(() => {
    countTotal();
  }, [ourCart])


  let fSetTrueCart = () => {
    let cart = JSON.parse(localStorage.getItem("cart")) || [];
    // console.log('cartPage.cart', cart);
    // setOurCart(cart);
    let cartTrue = [];
    cart.map((book, i) => {
      const index = props.products.findIndex((item) => item._id === book.id); 
      if(index === -1 ) {console.log('something')} else {cartTrue.push({...props.products[index], amount :book.amount})}
    })
    setOurCart([...cartTrue])
  }




  let increase = i => {
    let tempBook = ourCart;
    tempBook[i].amount += 1;
    setOurCart(tempBook, countTotal());

    let cart = JSON.parse(localStorage.getItem("cart")) || [];

    const index = cart.findIndex((item) => item.id === tempBook[i]._id);
    cart[index].amount += 1;
    localStorage.setItem("cart", JSON.stringify(cart))
  };

  let decrease = i => {
    let tempBook = ourCart;
    tempBook[i].amount -= 1;
    setOurCart(tempBook, countTotal());
    let cart = JSON.parse(localStorage.getItem("cart")) || [];

    const index = cart.findIndex((item) => item.id === tempBook[i]._id);
    cart[index].amount -= 1;
    localStorage.setItem("cart", JSON.stringify(cart))
  };

  let remove = i => {
    let tempCart = ourCart

    let cart = JSON.parse(localStorage.getItem("cart")) || [];
    const index = cart.findIndex((item) => item.id === tempCart[i]._id);
    cart.splice(index, 1);
    localStorage.setItem("cart", JSON.stringify(cart))

    tempCart.splice(i,1)
    setOurCart([...tempCart]);

    // console.log('i', i)

    }

    
  const CartProductsRenderer = ourCart.map((book, i) => {
    // console.log('cartpage ourcart: ', ourCart)
    return (
      <div key={i} className='cartProduct'>
        <div>
          <img src={book.cover.photo_url} alt='Cover' className='cartCoverPic'/>


          
        </div>

        <div>

          <h1>{book.title}</h1>
          <h2>{book.author}</h2>
          <h2>{book.genre}</h2>
          {book.bestseller === true && <h3 className= 'bestseller'>On the bestseller list!</h3>}

          <h3>Price: {book.price}$</h3> 
          <h3>Amount: {book.amount}</h3>
          
          <div>
            <button className='cartButtons' onClick={() => increase(i)}>+</button>
            <button className='cartButtons' disabled = {book.amount === 1} onClick={() => decrease(i)}>-</button>
            <button className='cartButtons' onClick ={() => remove(i)}>Remove book from cart</button>

          </div>
          <p>______________________________________________</p>

        </div>
      </div>
    );
  });

  return (
    <div className="cartSnippet">
      <section>
      <h1>Cart:</h1>
      {CartProductsRenderer}
      <p className="total">
          Total: <span>{total}</span>€
      </p>
      <button className='cartButtons' onClick={() => props.history.push({pathname: '/stripe', state: ourCart})}> next </button>
      </section>
    </div>
  );
};




export default Cart;
