import React, {useState, useEffect} from 'react';
import BookMapper from '../Renderers/BookMapper.js'; 

import Axios from 'axios';
import {URL} from '../config.js'

//! needs props === genre input

const GenrePage = (props) => {

    const [bookState, setBookState] = useState([])
      let getBook = async () => {
      try {
        let response = await Axios.get(`${URL}/products/genre/${props.match.params.genre}`)
        // console.log('response: ', [response.data])
        setBookState([...response.data.myProduct])
      } catch (error) {
        console.log(error)
      }
    }

      useEffect(() => {
        getBook()
      }, [props.match.params.genre])
    
      // console.log('Genre product page: ', props)
      return (
        <div>
          <h1 id="genrePageTitle">{props.match.params.genre}</h1>
          <section className='oneBooks'>
          <BookMapper {...props} Library={bookState}/>
          </section>    
        </div>
      )
    } 
export default GenrePage


