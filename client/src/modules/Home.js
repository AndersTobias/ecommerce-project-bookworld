import React, { useState, useEffect }from 'react';

import BookMapper from '../Renderers/BookMapper.js'; 
import ReviewMapper from '../Renderers/ReviewMapper.js';

import bookbanner from '../Misc/bookbanner.png'
import Axios from 'axios'
import {URL} from '../config.js'


const Home = (props) => {

  const [bookState, setBookState] = useState([]);
  const [bookStateOne, setBookStateOne] = useState([]);

  useEffect(() => {
    getBook()
  }, [])

  useEffect(() => {
    if(bookState.length !== 0)
    bookOne()
  }, [bookState])

  // useEffect(() => {
  //   console.log('Home: bookStateOne',bookStateOne, 'bookState: ', bookState)
  // }, [bookStateOne])

  let getBook = async () => {
    try {
      let response = await Axios.get(`${URL}/products/bestsellers`)
      // console.log('Home response: ', [response.data])
      setBookState([...response.data.myProduct])
    } catch (error) {
      console.log(error)
    }
  }

  let bookOne = () => {
    setBookStateOne([bookState[0]])
  }

  // useEffect(() => {
  //   console.log('Home bookState: ', bookState)
  // }, [bookState])

  return (
    <div id='home'>
      <h1 className='pageTitle'><u>Welcome to the BookWorld, where escapism is a virtue!</u></h1>
      <img id='bookbanner' src={bookbanner} alt='book banner'/>

      <section className='homeBestseller'>
        <h2>This weeks number one bestseller is:</h2>
        <BookMapper {...props} Library={bookStateOne}/>
      </section>

      <section>
        <ReviewMapper {...props} Library={bookStateOne}/>
      </section>


    </div>
  )
}

export default Home