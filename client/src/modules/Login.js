import React , { useState } from 'react'
import Axios from 'axios' 
import {URL} from '../config.js'


const Login = (props) => {
	const [ form , setValues ] = useState({
		email    : '',
		password : ''
	})
	const [ message , setMessage ] = useState('')
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
          const response = await Axios.post(`${URL}/users/login`,{
          	email:form.email,
          	password:form.password
          })
          setMessage(response.data.message)
          if( response.data.ok ){
              setTimeout( ()=> {
          props.login(response.data.token,response.data.id) //!-----
				  props.history.push('/')
			  },2000)    
          }
		}
        catch(error){
        	console.log(error)
        }
	}
	return <div className='CRUDadmin'>
					<h1>Login here</h1>
					<form onSubmit={handleSubmit}
									onChange={handleChange}
									className='CRUD'>
							<label>Email:</label>    
						<input name="email"/><br/>
						<label>Password:</label>
						<input name="password"/><br/>
						<button className='CRUDsubmit'>Login</button>
						<div className='message'><h4>{message}</h4></div>
						<div className='registerLine' onClick={() => props.history.push('/users/register')}>If you aren't already registered, then you can do that here</div>
					</form>
				</div>	
}
export default Login
