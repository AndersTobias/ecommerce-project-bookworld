import React , { useState } from 'react'
import Axios from 'axios' 
import {URL} from '../config.js'

//!needs more inputs:

const Register = (props) => {
	const [ form , setValues ] = useState({
		email    : '',
		password : '',
		password2: '',
		name     : '',
		address  : '',
		city     : '',
		postcode : '',
	})
	const [ message , setMessage ] = useState('')
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
			const response =  await Axios.post(`${URL}/users/register`,{
	            email: form.email,
			    password : form.password,
					password2: form.password2,
					name: form.name,
					address: form.address,
					city: form.city,
					postcode: form.postcode,
	        })
					setMessage(response.data.message)
					if(response.data.ok === true) {setTimeout(() => props.history.push(`/users`), 2000)
				}
		}
		catch( error ){
			console.log(error)
		}
	}
	return <div className='CRUDadmin'>
		    	<h1>User registration</h1>
    			<h3>all fields are necessary</h3>
					<form onSubmit={handleSubmit}
								onChange={handleChange}
								className='CRUD'>
						<label>Email:</label>
						<input name="email"/><br/>
						<label>Password:</label>
						<input name="password"/><br/>
						<label>Repeat password:</label>
						<input name="password2"/><br/>
						<label>Name:</label>
						<input name="name"/><br/>
						<label>Address:</label>
						<input name="address"/><br/>
						<label>City:</label>
						<input name="city"/><br/>
						<label>Postcode:</label>
						<input name="postcode"/><br/>
						<button className='CRUDsubmit'>Register</button>
						<div className='message'><h4>{message}</h4></div>
					</form>
				 </div>
}
export default Register
