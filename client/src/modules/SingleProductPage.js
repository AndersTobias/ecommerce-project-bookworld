import React, { useEffect, useState } from 'react';
// import Library from '../tempLibrary.js';
import BookMapper from '../Renderers/BookMapper.js'; 
import Axios from 'axios';
import {URL} from '../config.js'
import ReviewMapper from '../Renderers/ReviewMapper.js'
// import { json } from 'body-parser';
// import { response } from 'express';



const SingleProductPage = (props) => {


 const [bookState, setBookState] = useState([])
 const [reviewState, setReviewState] = useState(false)
 const [reviewStars, setReviewStars] = useState(1)
 const [user, setUser] = useState()
 const [actualReview , setActualReview ] = useState('')
 const [message , setMessage] = useState('')

  useEffect(() => {
    getBook()
    getUser()
  }, [])

  useEffect(() => {
   console.log('SingleProductPage.user: ', user, 'props.isloggedin', props.isLoggedIn)
 }, [user])

 useEffect(() => {
  console.log('SingleProductPage.bookstate: ', bookState)
}, [bookState])

  const handleChange = e => {
  setActualReview(e.target.value)
  }

  let postReview = async () => {
    // debugger;
    try {
      let response = await Axios.post(`${URL}/products/review/new`, {
        id: bookState[0]._id,
        user: user.name,
        review: actualReview,
        stars: reviewStars,
      })
      console.log('response SingleProductPage: ', response)
      setMessage(response.data.message)
    } catch (error) {
      console.log(error)
    }
  }

  let getUser = async () => {
    const response = await Axios.get(`${URL}/users/findUser`)
    console.log('SingleProductPage.getUser.response: ', response)
    setUser(response.data.myUser)
  }

  let getBook = async () => {
    try {
      let response = await Axios.get(`${URL}/products/${props.match.params.title}`)
      // console.log('response: ', [response.data])
      setBookState([...response.data.myProduct])
    } catch (error) {
      console.log(error)
    }
  }

  let setReview = (event) => {
    setReviewState(!reviewState);
  }


  let increase = (event) => {
    // console.log('increase', reviewStars)
    let tempReviewStars = reviewStars + 1;
    setReviewStars(tempReviewStars);
  }
  
  let decrease = (event) => {
    // console.log('decrease')
    let tempReviewStars = reviewStars - 1;
    setReviewStars(tempReviewStars);
  }

  // console.log('single product page: ', props)
  return (
    
    <div>
      <h1>{bookState.title}</h1>
      <section className='oneBooks'>
        <BookMapper {...props} Library={bookState}/>
      </section>
      <section className='reviews'>
        {props.isLoggedIn === true && <div>
          <button className='CRUDsubmit' onClick={setReview}>Click here to leave a review</button>
          </div>}

        {reviewState === true && <div>
        <h3>Just write your review here:</h3>
  
        <h4>How would you rate it from 1-5?</h4>
        <div className='reviewStarsContainer'>
          <button className='reviewsAddStarsButton' disabled = {reviewStars === 1} onClick={decrease}>-</button>
          <button className='reviewsAddStarsButton' disabled = {reviewStars === 5} onClick={increase}>+</button>

          <h3>{reviewStars}</h3>
        </div>
        <form onChange={handleChange} onSubmit={postReview}>
          <input className='reviewInput' name='reviewInput'/>
          <button className='CRUDsubmit' type='submit'>Submit your review</button>
        </form>
        </div>}
        <div className='CRUDmessage'><h4>{message}</h4></div>
        
        <ReviewMapper Library={bookState}/>
      </section>
    </div>
  )  
}

export default SingleProductPage