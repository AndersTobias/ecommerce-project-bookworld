import React, {useState, useEffect} from "react";
import axios from 'axios'
// import PaymentSuccess from "./payment_success";
import {URL} from '../config';
import OrderRenderer from '../Renderers/OrderRenderer.js'

const UserPage = (props) => {

  const [user, setUser] = useState({})
  const [orders, setOrders] = useState([])


  useEffect(() => {
    getUser();
    // console.log('userPage props: ',  props)
  }, [])

  useEffect(() => {
    // console.log('userPage user: ', user)
    getOrders();
  }, [user])


  let getUser = async () => {
    axios.defaults.headers.common["Authorization"] = JSON.parse(
      localStorage.getItem("token")
    );
    const response = await axios.get(`${URL}/users/findUser`);
    // console.log('userPage.getUser.response: ', response)
    setUser(response.data.myUser);
  };


  let getOrders = async () => {
    const response = await axios.get(`${URL}/users/orders/${user.email}`)
    // console.log('userPage.getOrders.response: ', response)
    setOrders(response.data.myProduct)
  }

  let orderMapper = (orders) => {
    let renderedOrder = orders.map((order, i) => {
      return <OrderRenderer key={i} {...props} products={props.products} order={order}/>
    })
    return <div className='orderContainer'>{renderedOrder}</div>
  }





      return(
        <div className='userPage'>
          <h1>HI, {user.name}!</h1>
          <div className='orderContainer'>
            <h2>-----------------------------------------</h2>
            <h3>These are your account details:</h3>
            <p>Name: {user.name}</p>
            <p>Email: {user.email}</p>
            <p>Address: {user.address}</p>
            <p>City: {user.city}</p>
            <p>Postcode: {user.postcode}</p>
            <h2>-----------------------------------------</h2>

            <button className='logoutButton' onClick={() => {props.history.push('/users'); props.logout()}}>Logout</button>
          </div>
          <div>
            <h3>These are your previous orders:</h3>
            {orderMapper(orders)}
          </div>
        </div>
      )

}

export default UserPage