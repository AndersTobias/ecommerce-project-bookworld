import React, { useState, useEffect } from "react";
import Products from "../Renderers/CheckoutProductRenderer.js";
import axios from 'axios'
import {URL} from '../config.js'
import { injectStripe } from "react-stripe-elements";



const Checkout = props => {

  console.log('props checkout: ', props);

  const [ourCart, setOurCart] = useState([]);

  const calculate_total = () => {
    let total = 0;
    ourCart.forEach(ele => (total += ele.quantity * ele.amount));
    return total;
  };

  useEffect(() => {
    let cart = JSON.parse(localStorage.getItem("cart")) || [];
    // console.log('localStorage.cart: ', cart);
    setOurCart(cart);
    nameSwap()
  }, []);

  let nameSwap = () =>{
    // console.log('nameSwap runs, this is ourCart: ', ourCart)
    let tempCart = [];
    props.location.state.map((book,i) => {
      let tempBook = {name: book.title, images: [book.cover.photo_url], amount: book.price, quantity: book.amount}
      tempCart.push(tempBook)
    })
    setOurCart(tempCart)
  }

  //=====================================================================================
  //=======================  CREATE CHECKOUT SESSION  ===================================
  //=====================================================================================
  const createCheckoutSession = async () => {
    try {
      const response = await axios.post(
        `${URL}/payments/create-checkout-session`,
        { products:ourCart }
      );
      // debugger
      return response.data.ok
        ? (localStorage.setItem(
            "sessionId",
            JSON.stringify(response.data.sessionId)
          ),
          redirect(response.data.sessionId))
        : props.history.push("/payment/error");
    } catch (error) {
      props.history.push("/payment/error");
    }
  };
  //=====================================================================================
  //=======================  REDIRECT TO STRIPE CHECKOUT  ===============================
  //=====================================================================================
  const redirect = sessionId => {
    // debugger;
    props.stripe
      .redirectToCheckout({
        // Make the id field from the Checkout Session creation API response
        // available to this file, so you can provide it as parameter here
        // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
        sessionId: sessionId
      })
      .then(function(result) {
        debugger;
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer
        // using `result.error.message`.
      });
  };
  //=====================================================================================
  //=====================================================================================
  //=====================================================================================
  return (
    <div className="checkout_container">
      <h1 className="checkout_header">Checkout - Check products before paying</h1>
      <div className="checkout_products_list">
        {ourCart.map((book, i) => {
          return <Products key={i} book={book} {...props}/>;
        })}
      </div>
      <div className="checkout_footer">
        <div className="checkout_total">Total : {calculate_total()} €</div>
        <button className='cartButtons' onClick={() => createCheckoutSession()}>
          PAY
        </button>
      </div>
    </div>
  );
};

export default injectStripe(Checkout);


