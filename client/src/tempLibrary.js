const Library = [
  {
    title: 'It',
    author: 'Stephen King',
    price: '2',
    wordCount: 390775,
    genre: 'Horror',
    description: 'Sometimes clown \'It\' waits for children in the sewers, it feels lonely and wants someone to play with',
    cover:'https://images-na.ssl-images-amazon.com/images/I/71hEBPMkCHL.jpg',
    bestseller: true,
    reviews: [
      {
        user: 'Another f-cking Stephen King Fan!',
        stars: 3,
        review: '5/7 would recommend!',
      },
    ],
  },
  {
    title: 'Watchmen',
    author: 'Alan Moore',
    price: '2',
    wordCount: 10000,
    genre: 'Sci-Fi',
    description: 'Superheroes aren\'t all what they seem to be',
    cover: 'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1442239711l/472331.jpg',
    bestseller: true,
    reviews: [
      {
        user: 'Moore_fan_69',
        stars: 5,
        review: 'I wanna marry Owlman!',
      },
    ],
  }
]

export default Library