const Products = require('../models/ProductModel');
require('dotenv').config();

//!---cloudinary
const cloudinary = require('cloudinary')

cloudinary.config({ 
    cloud_name: process.env.CLOUD_NAME, 
    api_key:    process.env.API_KEY, 
    api_secret: process.env.API_SECRET
  });
  console.log('process.env.CLOUD_NAME', process.env.CLOUD_NAME)
//!---cloudinary end

class ProductController {
    async findAll (req, res){
        try{
            const myProduct = await Products.find({});
            res.send({myProduct});
        }
        catch(error){
            res.send({error});
        };
    }

    async findOne (req, res){
        let { title } = req.params;
        try{
            const myProduct = await Products.find({ title: title })
            res.send({myProduct});
        }
        catch(error){
            res.send({error});
        };
    }

    async findOneId (req, res){
        let { id } = req.params;
        try{
            const myProduct = await Products.find({ _id: id })
            res.send({myProduct});
        }
        catch(error){
            res.send({error});
        };
    }

    async findBestsellers (req, res){
        try{
            const myProduct = await Products.find({ bestseller: true })
            // console.log('product controller findBestseller : ', myProduct)
            res.send({myProduct});
        }
        catch(error){
            console.log(error)
            res.send({error});
        };
    }

    async findGenre (req, res){
        let { genre } = req.params;
        try{
            const myProduct = await Products.find({ genre: genre })
            // console.log('product controller findGenre : ', myProduct)
            res.send({myProduct});
        }
        catch(error){
            console.log(error)
            res.send({error});
        };
    }

    async create (req, res){
        let{ genre, title, price, cover, description, wordCount, author, stock } = req.body;

        if(!title || !genre || !price || !cover || !description || !wordCount || !author || !stock) return res.json({ok:false, message:'All fields are required' }); //! <------ this for preventing empty input fields

        try{
            const myProduct = await Products.findOne({ title: title }); //! <---- these 2 lines are to check in DB if item already exists
            if( myProduct ) return res.json({ok:false, message: 'Book already exists'}); //! <---- be careful that "myProduct" and "newProduct" is not named the same-

            const newProduct = await Products.create({
                title: title,
                author: author,
                genre: genre,
                price: parseFloat(price),
                cover: cover,
                wordCount: wordCount,
                description: description,
                stock: stock, //! <----- Stock for the cart
                bestseller: false,
                reviews:[]
              });
              await Products.create(newProduct)  //!   <----- for the set message 
              res.json({ok:true,message:'Successfully added'}) //! <------------------ these 2 lines 
        }
        catch(error){
            console.log(error);
            res.send({error})
        }
    }

    async delete (req, res){
        console.log('delete!!!')
        let { title } = req.body;
        console.log('title:', title)
        try{
            const myProduct = await Products.findOne({ title: title })
            const removed = await Products.deleteOne({ title });
            console.log('myProduct.cover.public_id: ', myProduct.cover.public_id)
            await cloudinary.v2.api.delete_resources([ myProduct.cover.public_id ]);
            res.json({ok:true, message:'Successfully deleted'}) //! <------------------ these 2 lines 
        }
        catch(error){
            res.send({error});
        };
    }
    
    async update (req, res){
        let { title, newTitle, genre , price, cover, wordCount, description, author, stock} = req.body;

        if(!title || !genre || !price || !description || !wordCount || !author || !stock) return res.json({ok:false, message:'All fields are required' }); //! <------ this for preventing empty input fields

        try{
            const updated = await Products.updateOne(
                { title },{ title:newTitle,
                  genre: genre,
                  author: author,
                  price: parseFloat(price),
                  wordCount: wordCount,
                  description: description,
                  stock: stock, //! <---Stock for the cart
                }
             );
             await Products.updateOne(updated)  //!   <----- for the set message 
             res.json({ok:true,message:'Successfully updated'}) //! <------- these 2 lines 
        }
        catch(error){
            res.send({error});
        };
    }


   //! ----------------------------------------------------------------------------------------------------------this is where I am
    async addReview (req, res){
        let { id, review, stars, user} = req.body;
        if(!review) return res.json({ok:false, message:'All fields are required' }); //! <------ this for preventing empty input fields
        // if(!user) return res.json({ok:false, message:'you need to be logged in to leave a review' }); //! <------ this for preventing empty input fields
        try{
            const createReview = await Products.update( 
                { _id:id },{ $push: {reviews: {stars: stars, review: review, user: user}}}
             );
             await Products.update(createReview) 
             res.json({ok:true,message:'Review successfully added'}) 
        }
        catch(error){
            res.send({error});
        };
    }

};

module.exports = new ProductController();