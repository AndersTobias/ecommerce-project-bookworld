const User       = require('../models/UserModel'); 
const argon2     = require('argon2');//https://github.com/ranisalt/node-argon2/wiki/Options
const jwt        = require('jsonwebtoken');
const validator  = require('validator');
const jwt_secret = process.env.JWT_SECRET

const Orders = require('../models/OrderModel');


const register = async (req,res) => {
	const {name, email , password , password2, address, city, postcode } = req.body; 
	if(!name || !email || !password || !password2 || !address || !city || !postcode ) return res.json({ok:false,message:'All field are required'});
    if( password !== password2 ) return res.json({ok:false,message:'passwords must match'});
    if( !validator.isEmail(email) ) return res.json({ok:false,message:'please provide a valid email'})
    try{
    	const user = await User.findOne({ email })
    	if( user ) return res.json({ok:false,message:'email already in use'});
    	const hash = await argon2.hash(password)
        console.log('hash ==>' , hash)
        const newUser = {
            name: name,
        	email: email,
            password : hash,
            admin: false,
            address: address, 
            city: city,
            postcode: postcode,
        }
        await User.create(newUser)
        res.json({ok:true,message:'successful register'})
    }catch( error ){
        res.json({ok:false,error})
    }
}
const login = async (req,res) => {
    const { email , password } = req.body;
    if( !email || !password ) res.json({ok:false,message:'All field are required'});
    if( !validator.isEmail(email) ) return res.json({ok:false,message:'please provide a valid email'});
	try{
    	const user = await User.findOne({ email });
    	if( !user ) return res.json({ok:false,message:'plase provide a valid email'});
        const match = await argon2.verify(user.password, password);
        if(match) {
           const token = jwt.sign(user.toJSON(), jwt_secret ,{ expiresIn:100080 });//{expiresIn:'365d'}
           res.json({ok:true,message:'welcome back',token,email,id:user._id}) 
        }else return res.json({ok:false,message:'invalid password'})
    }catch( error ){
    	 res.json({ok:false,error})
    }
}

const verify_token = (req,res) => {
  console.log(req.headers.authorization)
  const token = req.headers.authorization;
       jwt.verify(token, jwt_secret, (err,succ) => {
             err ? res.json({ok:false,message:'something went wrong'}) : res.json({ok:true,succ})
       });      
    }

const findUser = async (req,res) => {
        console.log(req.headers.authorization)
        const token = req.headers.authorization;
            try{
                const decoded = await jwt.verify(token, jwt_secret)
                if(!decoded) {return res.send({ok:false, message:'No user found'})}
                const myUser = await User.findOne({ _id: decoded._id })
                // console.log('product controller findBestseller : ', myProduct)
                res.send({myUser});
            } catch(error) {
                res.json({ok:false, error})
            }
}

const findOrders = async (req, res) =>{
    let { email } = req.params;
    try{
        const myProduct = await Orders.find({ email: email })
        
        res.send({myProduct});
    }
    catch(error){
        res.send({error});
    };
}


//! This is another way to request the orders that also packages the products along with them. 
// const findOrders = async (req, res) =>{
//     let { email } = req.params;
//     // console.log('=======================',req.params)
//     try{
//         const myProduct = await Orders.find({ email: email })
//         const allProducts = await Products.find({})
//         const temp = []
//         myProduct.forEach( ele => {
//             const tempCart = []
//             ele.cart.forEach( cartItem => {
//                 let index = allProducts.findIndex( item => item._id == cartItem.id)
//                 tempCart.push(allProducts[index])
//             })
//             ele.cart = tempCart
//             temp.push(ele)
//         })
//         console.log('================>', temp)
//         res.send({myProduct:temp});
//     }
//     catch(error){
//         res.send({error});
//     };
//! End-------------------------------------------------------------------------------------

module.exports = { register , login , verify_token, findUser, findOrders }



