const Orders = require('../models/OrderModel.js');
const Users = require('../models/UserModel.js');
const Products = require('../models/ProductModel');


// success_url: `${domainURL}/payment/success?session_id={CHECKOUT_SESSION_ID}`

require('dotenv').config();

const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

const create_checkout_session = async (req, res) => {
  console.log(process.env.STRIPE_SECRET_KEY);
  try {
    const domainURL = process.env.DOMAIN;
    const { products } = req.body;
    if (products.length < 1 || !products)
      return res.send({
        ok: false,
        message: "Please select at least 1 product"
      });
    products.forEach(item => {
      item.currency = process.env.CURRENCY;
      item.amount *= 100;
    });
    // Create new Checkout Session for the order
    // Other optional params include:
    // [billing_address_collection] - to display billing address details on the page
    // [customer] - if you have an existing Stripe Customer ID
    // [payment_intent_data] - lets capture the payment later
    // [customer_email] - lets you prefill the email input in the form
    // For full details see https://stripe.com/docs/api/checkout/sessions/create
    session = await stripe.checkout.sessions.create({
      payment_method_types: process.env.PAYMENT_METHODS.split(", "),
      line_items: products,
      // ?session_id={CHECKOUT_SESSION_ID} means the redirect will have the session ID set as a query param
      success_url: `${domainURL}/payment/success?session_id={CHECKOUT_SESSION_ID}`,
      cancel_url: `${domainURL}/`
    });
    return res.send({ ok: true, sessionId: session.id });
  } catch (error) {
    console.log("ERROR =====>", error);
    return res.send({ ok: false, message: error });
  }
};

const checkout_session = async (req, res) => {
  console.log(req.query.sessionId)
  try {
    const { sessionId } = req.query;
    const session = await stripe.checkout.sessions.retrieve(sessionId);
    const customer = await stripe.customers.retrieve(session.customer);
    // const user = await Users.findOne({})

    let total = 0;

    session.display_items.map((item, i ) => {
      total += item.amount * item.quantity;
    })

    console.log('payment controller session: ', session, ' customer.email: ', customer.email, ' req.body.cart: ', req.body.cart, ' total: ', total )


    let createdObj = await Orders.create({email: customer.email, cart:req.body.cart, totalPrice: total/100 })

    const products = await Products.find({});
    await Promise.all(req.body.cart.map(item => Products.updateOne(
      { _id: item.id},
      { $inc: { stock: -Math.abs(item.amount) } }
    )));

    return res.send({ ok: true, session: session, customer });
  } catch (error) {
    console.log("Payments.controller ERROR =====>", error);
    return res.send({ ok: false, message: error });
  }
};

module.exports = {
  create_checkout_session,
  checkout_session
};
