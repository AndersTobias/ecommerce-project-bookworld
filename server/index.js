const express = require('express');
const app = express(),
    mongoose = require('mongoose'),
    ProductRoute = require('./routes/ProductRoute'),
    PaymentRoute = require('./routes/PaymentRoute.js'),
    OrderRoute = require('./routes/OrderRoute')

    bodyParser = require('body-parser');
    const PORT = process.env.PORT || 3001
    const path = require('path');


//! initial settings 
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
let cors = require('cors')
app.use(cors())
require('dotenv').config();

//! mongo set up

async function connecting(){
  try {console.log(process.env.ATLAS_URL)
      await mongoose.connect(process.env.ATLAS_URL, { useUnifiedTopology: true , useNewUrlParser: true })
      console.log('Connected to the DB')
  } 
  catch ( error ) {
      console.log('ERROR: Seems like your DB is not running, please start it up !!!');
    }
  }

connecting()

mongoose.set('useCreateIndex', true);
// end

//! mail
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");

	next();
});

// end

//! routes 
app.use('/products', ProductRoute);
app.use('/payments', PaymentRoute);
app.use('/orders', OrderRoute);
app.use('/emails', require('./routes/EmailsRoute.js'))
app.use('/users', require('./routes/UserRoute.js'))
// end

//! deployment
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../client/build')));
app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
});
// end

app.listen(PORT, function() {
	console.log(`Serving my master on port ${PORT}!`)
})