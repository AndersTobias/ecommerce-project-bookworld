const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const orderSchema = new Schema({ 
      cart: Array, 
      email: String,      
      totalPrice: Number,
      date: {type:Date, default:Date.now} 
});
module.exports = mongoose.model('order', orderSchema);