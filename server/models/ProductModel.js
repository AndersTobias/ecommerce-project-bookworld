const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bookSchema = new Schema({ 
      genre:String, 
      title:String, 
      price:Number, 
      author:String, 
      description:String,
      cover: Object, //!----
      wordCount: Number,
      bestseller: Boolean,
      stock: Number,  //! <--stock for the cart
      reviews: [
        {
          user: String,
          stars: Number,
          review: String,
        }
      ] 
});
module.exports = mongoose.model('books', bookSchema);