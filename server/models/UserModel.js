const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const userSchema = new Schema({ 
      email: { type:String, unique:true, required:true }, 
      password: { type:String, unique:true, required:true }, 
      name: String, 
      address: String, 
      city: String,
      postcode: Number,
      admin: { type:Boolean, required:true },
});
module.exports = mongoose.model('users', userSchema);