const express = require('express'), router = express.Router(), controller = require('../controllers/ProductController');

router.get('/', controller.findAll);

router.post('/delete', controller.delete);

router.post('/update', controller.update);

router.post('/new', controller.create);

router.get('/:id', controller.findOne);

module.exports  = router;





