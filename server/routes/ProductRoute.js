const express = require('express'), router = express.Router(), controller = require('../controllers/ProductController');

router.get('/', controller.findAll);



router.post('/delete', controller.delete);

router.post('/update', controller.update);

// router.post('/updatestock', controller.updateStock);

router.post('/new', controller.create);

router.post('/review/new', controller.addReview);

router.get('/bestsellers', controller.findBestsellers);

router.get('/genre/:genre', controller.findGenre);

router.get('/book/:id', controller.findOneId);

router.get('/:title', controller.findOne);


module.exports  = router;





