const router = require('express').Router();
const controller = require('../controllers/UserControllers')

router.post('/register',controller.register);
router.post('/login',controller.login);
router.post('/verify_token',controller.verify_token);
router.get('/findUser',controller.findUser)
router.get('/orders/:email', controller.findOrders);


module.exports = router;